extern crate reqwest;

use std::cmp::min;
use std::io;
use std::sync::{Arc, Mutex};
use std::sync::mpsc;
use std::thread;
use std::time::Duration;

use pbr::ProgressBar;

use purecloud_api::login;
use purecloud_api::users;

use errors::*;

/// Progress bar.
pub struct PBar {
    progress_bar: Arc<Mutex<ProgressBar<io::Stdout>>>,
    silent: bool,
    join_handle: Option<thread::JoinHandle<()>>,
    tx: mpsc::Sender<()>,
}

impl PBar {
    pub fn new(count: u64, silent_mode: bool) -> PBar {
        let mut pb = ProgressBar::new(count);
        pb.tick_format("\\|/-");
        pb.format("|#--|");
        pb.show_tick = true;
        pb.show_speed = false;
        pb.show_percent = true;
        pb.show_counter = true;
        pb.show_time_left = false;

        // Channel used to communicate status between threads.
        let (tx, rx) = mpsc::channel::<()>();
        let pbb = Arc::new(Mutex::new(pb));
        let pbbb = pbb.clone();

        let h = if silent_mode {
            None
        } else {
            Some(thread::spawn(move || {
                loop {
                    match rx.recv_timeout(Duration::from_millis(100)) {
                        Ok(..) => return,
                        Err(..) => (),
                    };

                    match pbbb.lock() {
                        Ok(mut p) => p.tick(),
                        Err(..) => (),
                    };
                }
            }))
        };

        PBar {
            progress_bar: pbb,
            silent: silent_mode,
            join_handle: h,
            tx: tx,
        }
    }

    pub fn inc(&mut self, count: u64) {
        if !self.silent {
            match self.progress_bar.lock() {
                Ok(mut p) => {
                    p.add(count);
                    ()
                } // ignore the u64 result from inc.
                Err(..) => (),
            };
        }
    }

    pub fn finish(self) {
        if !self.silent {
            self.tx.send(()).unwrap_or_else(|e| println!("{}", e.to_string()));
            match self.progress_bar.lock() {
                Ok(mut p) => p.finish_println("\n"),
                Err(..) => println!("\n"),
            };
            match self.join_handle {
                Some(t) => t.join().unwrap_or(()),
                None => (),
            };
        }
    }
}

pub struct Agent<'a> {
    pub client: &'a reqwest::Client,
    pub creds: login::Credentials,
    pub auth_token: String,
    pub silent: bool,
}

impl<'a> Agent<'a> {
    pub fn new(client: &'a reqwest::Client,
               creds: login::Credentials,
               silent_mode: bool)
               -> Agent<'a> {
        Agent {
            client: client,
            creds: creds,
            auth_token: String::new(),
            silent: silent_mode,
        }
    }

    pub fn login(&mut self) -> Result<()> {
        let req = login::LoginRequestOptions {
            creds: &self.creds,
            client: self.client,
        };

        let auth_token = login::login(&req)?;
        self.auth_token = auth_token;

        Ok(())
    }

    /// Calculate the number of pages to receive. This is based off the page
    /// count and user count provided at startup as well as the total number
    /// of users that could possibly be retrieved.
    fn calc_page_count(total: u32, page_size: u32, user_count: u32) -> u64 {
        let a = (total - page_size) as f32 / page_size as f32;
        let b = a.ceil() as u64;
        let total_pages = b + 1;

        if user_count == 0 {
            total_pages
        } else {
            let a = user_count as f32 / page_size as f32;
            let b = a.ceil() as u64;
            min(total_pages, b)
        }
    }

    pub fn get_users(&self,
                     page_size: u32,
                     user_count: Option<u32>)
                     -> Result<Vec<users::Person>> {
        let mut req = users::GetUserRequestOptions {
            url: None,
            page_size: page_size,
            auth_token: &self.auth_token,
            client: self.client,
        };

        let mut result = users::get_users(&req).chain_err(|| "failed to get users")?;

        let user_count_to_get = match user_count {
            Some(n) => n,
            None => result.total,
        };

        let mut people = result.entities
            .into_iter()
            .collect();

        let mut results = Vec::<users::Person>::new();
        results.append(&mut people);

        let mut n_url = result.next_uri.clone();

        // let page_count = Agent::calc_page_count(result.total, page_size, user_count_to_get);
        let mut pb = PBar::new(user_count_to_get as u64, self.silent);
        pb.inc(0 as u64);

        while let Some(next_url) = n_url {
            if results.len() >= user_count_to_get as usize {
                break;
            }

            // Create the next request using the 'next url' retrieved from the
            // previous call.
            req = users::GetUserRequestOptions { url: Some(next_url.to_string()), ..req };

            result = users::get_users(&req).chain_err(|| "failed to get users")?;

            pb.inc(result.entities.len() as u64);

            n_url = result.next_uri.clone();

            let mut people = result.entities
                .into_iter()
                .collect();
            results.append(&mut people);
        }

        pb.finish();

        results.sort_by_key(|v| v.name.clone());
        results.sort_by(|a, b| a.name.cmp(&b.name));

        Ok(results)
    }
}
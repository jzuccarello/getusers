extern crate clap;

use std::fmt;
use std::io;
use std::io::Write;

use clap::ArgMatches;
use rpassword::read_password;

use errors::*;

pub const PAGE_SIZE_DEFAULT: u32 = 25;

pub enum ResultFormat {
    Display,
    Json,
    Dsv,
}

impl fmt::Display for ResultFormat {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let text = match *self {
            ResultFormat::Display => "display",
            ResultFormat::Json => "json",
            ResultFormat::Dsv => "dsv",
        };
        write!(f, "{}", text)
    }
}

pub enum SortField {
    Name,
    Email,
    Id,
}

impl fmt::Display for SortField {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let text = match *self {
            SortField::Name => "name",
            SortField::Email => "email",
            SortField::Id => "id",
        };
        write!(f, "{}", text)
    }
}

pub enum SortOrder {
    Asc,
    Dsc,
}

impl fmt::Display for SortOrder {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let text = match *self {
            SortOrder::Asc => "ascending",
            SortOrder::Dsc => "descending",
        };
        write!(f, "{}", text)
    }
}

pub struct AppOpts {
    pub org: String,
    pub email: String,
    pub password: String,
    pub page_size: u32,
    pub count: Option<u32>,
    pub silent: bool,
    pub sort_field: SortField,
    pub sort_order: SortOrder,
    pub format: ResultFormat,
}

pub fn process_matches(matches: &ArgMatches) -> Result<AppOpts> {
    let opts = AppOpts {
        org: get_org(matches),
        email: get_org_email(matches),
        password: get_org_password(matches)?,
        page_size: get_page_size(matches)?,
        count: get_user_count(matches)?,
        silent: get_silent(matches),
        sort_field: get_sort_field(matches),
        sort_order: get_sort_order(matches),
        format: get_format(matches),
    };

    Ok(opts)
}

pub fn calc_user_count(opts_count: Option<u32>, result_count: usize) -> usize {
    match opts_count {
        Some(n) => n as usize,
        None => result_count,
    }
}

fn get_org(matches: &ArgMatches) -> String {
    // Ok to unwrap here since the arg parser will require that the org
    // param is provided or else the app quits.
    matches.value_of("org").unwrap().to_string()
}

fn get_org_email(matches: &ArgMatches) -> String {
    // Ok to unwrap here since the arg parser will require that the org email
    // param is provided or else the app quits.
    matches.value_of("email").unwrap().to_string()
}

fn get_org_password(matches: &ArgMatches) -> Result<String> {
    match matches.value_of("password") {
        Some(p) => Ok(p.to_string()),
        None => {
            print!("Enter password: ");
            let _ = io::stdout().flush();
            match read_password() {
                Ok(p) => {
                    if p.is_empty() {
                        Err("password must be provided".into())
                    } else {
                        Ok(p)
                    }
                },
                Err(e) => Err(e.to_string().into())
            }
        }
    }
}

fn get_page_size(matches: &ArgMatches) -> Result<u32> {
    match matches.value_of("pagesize") {
        Some(x) => Ok(x.parse::<u32>().chain_err(|| "unable to parse <pagesize> param")?),
        None => Ok(PAGE_SIZE_DEFAULT)
    }
}

fn get_user_count(matches: &ArgMatches) -> Result<Option<u32>> {
    match matches.value_of("count") {
        Some(x) => {
            let num = x.parse::<u32>().chain_err(|| "unable to parse <count> param")?;
            Ok(Some(num))
        },
        None => Ok(None)
    }
}

fn get_sort_order(matches: &ArgMatches) -> SortOrder {
    match matches.value_of("sort-order") {
        Some(o) => {
            match o.to_lowercase().as_ref() {
                "asc" => SortOrder::Asc,
                "dsc" => SortOrder::Dsc,
                _ => SortOrder::Asc,
            }
        }
        None => SortOrder::Asc,
    }
}

fn get_sort_field(matches: &ArgMatches) -> SortField {
    match matches.value_of("sort-field") {
        Some(f) => {
            match f.to_lowercase().as_ref() {
                "name" => SortField::Name,
                "email" => SortField::Email,
                "id" => SortField::Id,
                _ => SortField::Name,
            }
        }
        None => SortField::Name,
    }
}

fn get_format(matches: &ArgMatches) -> ResultFormat {
    match matches.value_of("format") {
        Some(f) => {
            match f.to_lowercase().as_ref() {
                "display" => ResultFormat::Display,
                "json" => ResultFormat::Json,
                "dsv" => ResultFormat::Dsv,
                _ => ResultFormat::Display,
            }
        }
        None => ResultFormat::Display,
    }
}

fn get_silent(matches: &ArgMatches) -> bool {
    matches.is_present("silent")
}
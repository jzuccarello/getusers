extern crate reqwest;
extern crate serde_json;

use std::cmp;
use std::io::Read;

use itertools::Itertools;
use reqwest::header::{Bearer, ContentType, Authorization, Headers};
use retry::Retry;

use errors::*;

use super::core;
use super::super::opts::SortOrder;

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Person {
    pub id: String,
    pub name: String,
    pub email: String,
}

/// Macro used to determine which field comparison to use based on SortOrder.
/// It might be clearer just to have the match statement in use instead of the
/// macro, but the only change between match expression useage are the fields
/// being compared. So this keeps it DRY, reduces some code lines and....fun to use.
macro_rules! compare_by_sort_order {
    ($order:expr, $asc_op:expr, $dsc_op:expr) => {
        match $order {
            SortOrder::Asc => $asc_op,
            SortOrder::Dsc => $dsc_op,
        }
    }
}

// pub fn sort_by_name(data: &Vec<Person>, order: &SortOrder) -> Vec<Person> {
pub fn sort_by_name(data: &[Person], order: &SortOrder) -> Vec<Person> {
    let f = |a: &Person, b: &Person, order: &SortOrder| {
        compare_by_sort_order!(*order, (&a.name).cmp(&b.name), (&b.name).cmp(&a.name))
    };
    sort_by(f, data, order)
}

// pub fn sort_by_email(data: &Vec<Person>, order: &SortOrder) -> Vec<Person> {
pub fn sort_by_email(data: &[Person], order: &SortOrder) -> Vec<Person> {
    let f = |a: &Person, b: &Person, order: &SortOrder| {
        compare_by_sort_order!(*order, (&a.email).cmp(&b.email), (&b.email).cmp(&a.email))
    };
    sort_by(f, data, order)
}

// pub fn sort_by_id(data: &Vec<Person>, order: &SortOrder) -> Vec<Person> {
pub fn sort_by_id(data: &[Person], order: &SortOrder) -> Vec<Person> {
    let f = |a: &Person, b: &Person, order: &SortOrder| {
        compare_by_sort_order!(*order, (&a.id).cmp(&b.id), (&b.id).cmp(&a.id))
    };
    sort_by(f, data, order)
}

// fn sort_by<F>(mut pred: F, data: &Vec<Person>, order: &SortOrder) -> Vec<Person>
fn sort_by<F>(mut pred: F, data: &[Person], order: &SortOrder) -> Vec<Person>
    where F: FnMut(&Person, &Person, &SortOrder) -> cmp::Ordering
{
    data.into_iter()
        .sorted_by(|a, b| pred(a, b, order))
        .into_iter()
        .map(|x| x.clone())
        .collect()
}

pub struct GetUserRequestOptions<'a> {
    pub url: Option<String>,
    pub page_size: u32,
    pub auth_token: &'a String,
    pub client: &'a reqwest::Client,
}

pub fn get_users(options: &GetUserRequestOptions) -> Result<core::PagedResponse<Person>> {
    let url: String = match options.url {
        Some(ref u) => format!("https://api.inintca.com{}", u),
        None => {
            let base_url = "https://api.inintca.com/api/v2/users".to_string();
            match options.page_size {
                0 => return Err("page size must be greater than 0".into()),
                25 => base_url,
                n => format!("{}?pageSize={}", base_url, n),
            }
        }
    };

    /// Call the 'users' endpoint and if the rate limit is exceeded, then
    /// exponentially backoff and retry.
    let res = Retry::new(&mut || {
        let mut headers = Headers::new();
        headers.set(ContentType::json());
        headers.set(Authorization(Bearer { token: options.auth_token.clone() }));
        headers.set(core::ININAuthAPI(options.auth_token.clone()));

        options.client.get(&url).headers(headers).send().unwrap()
    }, &mut |value| value.status().is_success())
        .wait_exponentially(3.0)
        .try(20)
        .execute()
        .map_err(|e| e.to_string());

    /// Get the response if it was successful.
    let mut r = res?;
    let mut s = String::new();
    r.read_to_string(&mut s).chain_err(|| "failed to read response body")?;
    let page_data = serde_json::from_str(&s).chain_err(|| "failed to deserialize response body")?;

    Ok(page_data)
}
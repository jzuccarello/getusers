#[macro_export]
header! { (ININAuthAPI, "ININ-Auth-API") => [String] }

#[macro_export]
header! { (ININRateLimitAllowed, "inin-ratelimit-allowed") => [u32] }

#[macro_export]
header! { (ININRateLimitCount, "inin-ratelimit-count") => [u32] }

#[macro_export]
header! { (ININRateLimitReset, "inin-ratelimit-reset") => [u32] }

#[derive(Serialize, Deserialize, Debug)]
pub struct PagedResponse<T> {
    pub entities: Vec<T>,

    #[serde(rename="pageSize")]
    pub page_size: u32,

    #[serde(rename="pageNumber")]
    pub page_number: u32,

    pub total: u32,

    #[serde(rename="selfUri")]
    pub self_uri: String,

    #[serde(rename="firstUri")]
    pub first_uri: String,

    #[serde(rename="nextUri")]
    pub next_uri: Option<String>,

    #[serde(rename="previousUri")]
    pub previous_uri: Option<String>,

    #[serde(rename="lastUri")]
    pub last_uri: String,

    #[serde(rename="pageCount")]
    pub page_count: u32,
}
extern crate reqwest;
extern crate serde_json;

use std::io::Read;
use std::collections::HashMap;

use hyper::header::{Headers, ContentType};

use errors::*;

#[derive(Serialize, Deserialize, Debug)]
pub struct LoginResponse {
    pub res: LoginAuthDetails,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct LoginAuthDetails {
    #[serde(rename="X-OrgBook-Auth-Key")]
    pub x_orgbook_auth_key: String,
}

#[derive(Deserialize)]
struct LoginFailure {
    status: u32,
}

pub struct Credentials {
    pub email: String,
    pub password: String,
    pub org_name: String,
}

pub struct LoginRequestOptions<'a> {
    pub creds: &'a Credentials,
    pub client: &'a reqwest::Client,
}

/// Calls the PureCloud login endpoint and returnts the auth key if successful.
pub fn login(options: &LoginRequestOptions) -> Result<String> {
    let mut map = HashMap::new();
    map.insert("orgName".to_string(), &options.creds.org_name);
    map.insert("email".to_string(), &options.creds.email);
    map.insert("password".to_string(), &options.creds.password);

    let mut headers = Headers::new();
    headers.set(ContentType::json());

    let mut res = options.client
        .post("https://apps.inintca.com/api/v2/login")
        .headers(headers)
        .json(&map)
        .send()
        .chain_err(|| "failed to login")?;

    let mut s = String::new();

    // Failed login attempt.
    if !res.status().is_success() {
        res.read_to_string(&mut s).chain_err(|| "failed to read response body")?;
        let failure_res: LoginFailure =
            serde_json::from_str(&s).chain_err(|| "failed to deserialize response body")?;
        return Err(format!("Failed to login. Response status code: {}",
                           failure_res.status).into());
    }

    res.read_to_string(&mut s).chain_err(|| "failed to read response body")?;

    let login_res: LoginResponse =
        serde_json::from_str(&s).chain_err(|| "failed to deserialize response body")?;

    Ok(login_res.res.x_orgbook_auth_key)
}
extern crate serde_json;

use std;
use std::cmp::max;

use purecloud_api::users;

pub fn write_json_to_stdout(results: &Vec<users::Person>) {
    let s = serde_json::to_string(results).unwrap_or("error converting to json".to_string());
    println!("{:?}", s);
}

pub fn write_dsv_to_stdout(results: &[users::Person]) {
    println!("Number|Username|Email|Id");

    for (i, person) in results.into_iter().enumerate() {
        println!("{}|{}|{}|{}", i + 1, person.name, person.email, person.id);
    }
}

pub fn write_table_to_stdout(results: &[users::Person]) {
    let ref mut iter = results.into_iter();

    // Process all lines in the results. Determine the maximum width for each
    // field as that will become a column in the output and this will allow for
    // correct width alignment and size for each column.
    let col_widths = iter.enumerate()
        .fold((0, 0, 0, 0), |acc, (i, p)| {
            let idx_count = i.to_string().chars().count();
            let name_count = p.name.chars().count();
            let email_count = p.email.chars().count();
            let id_count = p.id.chars().count();
            (max(acc.0, idx_count),
             max(acc.1, name_count),
             max(acc.2, email_count),
             max(acc.3, id_count))
        });
    let idx_width = max(col_widths.0, 6);
    let name_width = col_widths.1;
    let email_width = col_widths.2;
    let id_width = col_widths.3;

    println!("{0: <idx_width$} | {1: <name_width$} | {2: <email_width$} | {3: <id_width$}",
             "Number",
             "Username",
             "Email",
             "Id",
             idx_width = idx_width,
             name_width = name_width,
             email_width = email_width,
             id_width = id_width);
    println!("{}",
             std::iter::repeat("-")
                 // 9 == the | characters and spaces in the header above
                 .take(idx_width + name_width + email_width + id_width + 9)
                 .collect::<String>());

    for (i, user) in results.into_iter()
        .enumerate() {
        println!("{0: <idx_width$} | {1: <name_width$} | {2: <email_width$} | {3: <id_width$}",
                 i + 1,
                 user.name,
                 user.email,
                 user.id,
                 idx_width = idx_width,
                 name_width = name_width,
                 email_width = email_width,
                 id_width = id_width);
    }
}
#![feature(custom_derive)]
#![feature(proc_macro)]
#![recursion_limit = "1024"]

#[macro_use]
extern crate hyper;
#[macro_use]
extern crate serde_derive;
#[macro_use]
extern crate error_chain;
extern crate clap;
extern crate pbr;
extern crate retry;
extern crate rpassword;
extern crate serde_json;
extern crate serde;
extern crate itertools;
extern crate reqwest;

mod errors {
    error_chain! {}
}

mod agent;
mod opts;
mod output;
mod purecloud_api;

use clap::{Arg, App};

use errors::*;
use opts::ResultFormat;
use purecloud_api::{login, users};

fn main() {
    if let Err(ref e) = run() {
        println!("error: {}", e);

        for e in e.iter().skip(1) {
            println!("caused by: {}", e);
        }

        // The backtrace is not always generated. Try to run this example
        // with `RUST_BACKTRACE=`.
        if let Some(backtrace) = e.backtrace() {
            println!("backtrace: {:?}", backtrace);
        }

        ::std::process::exit(1);
    }
}

fn sort_users(data: &Vec<users::Person>,
              field: opts::SortField,
              order: opts::SortOrder)
              -> Vec<users::Person> {
    match field {
        opts::SortField::Name => users::sort_by_name(data, &order),
        opts::SortField::Email => users::sort_by_email(data, &order),
        opts::SortField::Id => users::sort_by_id(data, &order),
    }
}

fn process_cmd_options() -> Result<opts::AppOpts> {
    let matches = App::new("getusers")
        .version("0.1.0")
        .about("Get users from PureCloud and display the name, email and id fields.")
        .arg(Arg::with_name("org")
            .required(true)
            .help("Org name to get users from")
            .takes_value(true))
        .arg(Arg::with_name("email")
            .required(true)
            .help("Admin email address")
            .takes_value(true))
        .arg(Arg::with_name("password")
            .help("User's password. If not provided, program will prompt.")
            .takes_value(true))
        .arg(Arg::with_name("pagesize")
            .short("p")
            .long("pagesize")
            .help("Page size on each request")
            .default_value("25")
            .takes_value(true)
            .display_order(1))
        .arg(Arg::with_name("count")
            .short("n")
            .long("count")
            .help("Number of users to retrieve. If not provided all are retrieved.")
            .takes_value(true)
            .display_order(2))
        .arg(Arg::with_name("silent")
            .short("s")
            .long("silent")
            .help("Don't display program messages"))
        .arg(Arg::with_name("sort-field")
            .long("sort-field")
            .help("Field to sort on")
            .takes_value(true)
            .value_name("field")
            .possible_values(&["name", "email", "id"]))
        .arg(Arg::with_name("sort-order")
            .long("sort-order")
            .help("Sort order")
            .takes_value(true)
            .value_name("order")
            .possible_values(&["asc", "dsc"]))
        .arg(Arg::with_name("format")
            .short("f")
            .long("format")
            .help("Format options for the results")
            .takes_value(true)
            .value_name("format")
            .possible_values(&["display", "json", "dsv"]))
        .get_matches();

    let opts = opts::process_matches(&matches)?;

    if !opts.silent {
        println!("\nRetrieving users:");
        println!("- org:        {}", opts.org);
        println!("- email:      {}", opts.email);
        println!("- pagesize:   {:?}", opts.page_size);
        let user_count_str = match opts.count {
            Some(n) => n.to_string(),
            None => "<all users>".to_string(),
        };
        println!("- count:      {}", user_count_str);
        println!("- sort-field: {}", opts.sort_field);
        println!("- sort-order: {}", opts.sort_order);
        println!("- format:     {}", opts.format);

        println!("\nRequesting users...");
    }

    Ok(opts)
}

fn run() -> Result<()> {
    let opts = process_cmd_options()?;

    let client = reqwest::Client::new().chain_err(|| "unable to create new http client")?;

    let creds = login::Credentials {
        email: opts.email,
        password: opts.password,
        org_name: opts.org,
    };

    let mut agent = agent::Agent::new(&client, creds, opts.silent);

    agent.login()?;

    let users = agent.get_users(opts.page_size, opts.count)?;

    let user_n = opts::calc_user_count(opts.count, users.len());
    let user_list = users.into_iter()
        .take(user_n)
        .collect();
    let sorted_users = sort_users(&user_list, opts.sort_field, opts.sort_order);

    match opts.format {
        ResultFormat::Display => output::write_table_to_stdout(&sorted_users),
        ResultFormat::Json => output::write_json_to_stdout(&sorted_users),
        ResultFormat::Dsv => output::write_dsv_to_stdout(&sorted_users),
    };

    Ok(())
}
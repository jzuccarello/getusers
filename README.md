# Get users from PureCloud

Command line utility to retrieve users from a PureCloud org written in [Rust](https://www.rust-lang.org/).

## Features
* Multiple output formats: [console, JSON, DSV]
* Sort users based on user name, email, or id (after retrieval)
* Automatically backoff and retry requests if 429'd

![Screenshot](./images/rec_1.gif)

## Why create this?
* Kept looking up users to associate name with email through raw api calls or on the admin page
* Needed the users GUID
* Wanted to try out Rust which has a strong story on safety and see what the dev experience was like for a little CLI app
* Learn something new

## Why not Go?
Go would be a *great* language to have written this in. Go and Rust compile down to a single binary which is helpful
for a CLI app. Go also has a couple great libraries for command line arg parsing. Ultimately, I wanted to test out the
ergonomics of Rust and try something new.

## CLI Options

Here's a screenshot of the options available:

![Help Screen](./images/help_screen.png)

## Building

This should build cross-platform with little to no issues. There is a Windows binary included in the repo. MacOS and
linux users will need to build manually.

The Windows binary was built with compiler version **1.16.0-nightly (4ecc85beb 2016-12-28)** on Windows 7.

When building in *release* mode, link-time optimization (LTO) is enabled in the **cargo.toml** file.
This will increase compile time, but the resulting executable *should* be smaller than if it wasn't enabled.